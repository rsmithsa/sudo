﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Richard Smith">
//     Copyright (c) Richard Smith. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace sudo
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Main program class for the console application.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Entry point for the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        private static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                try
                {
                    var startInfo = new ProcessStartInfo(args[0], string.Join(" ", args.Skip(1)));
                    startInfo.Verb = "runas";
                    Process.Start(startInfo);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("An error occured");
                    Console.WriteLine(ex);
                }
            }
            else
            {
                Console.WriteLine("sudo 1.0.1.0 Copyright © Richard Smith 2015");
                Console.WriteLine("Usage: sudu command [parameters]");
            }
        }
    }
}
