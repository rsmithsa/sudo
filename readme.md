sudo
====

A simple command line tool to run elevated processes on Windows.

Usage
-----

sudo 1.0.1.0 Copyright © Richard Smith 2015
sudu command [parameters]
